import { IsNotEmpty, IsInt } from 'class-validator';
class CreateOrderItemDto {
  @IsNotEmpty()
  @IsInt()
  productId: number;

  @IsNotEmpty()
  @IsInt()
  amount: number;
}

export class CreateOrderDto {
  @IsNotEmpty()
  @IsInt()
  customerId: number;

  @IsNotEmpty()
  orderItems: CreateOrderItemDto[];
}
